# Informations générales sur GitLab

## Structure des projets

Votre compte GitLab permet de créer plusieurs projets. Chaque projet peut contenir plusieurs fichiers, et regroupe toutes les informations associées: liste des contributeurs, commentaires sur les fichiers, etc.

## Taper du texte

Pour que les fichiers soient faciles à lire, il faut que leur nom se termine pad `.txt`ou `.md`. Si vous choisissez `.md`, il est possible de mettre un peu de présentation.

Quand un fichier est affiché, il est possible d'alterner entre sa forme brute et sa forme présentée avec le bouton `</>` dont l'étiquette dit « display source / display rendered file ».

Examinez la forme brute du présent fichier pour voir comment la présentation est obtenue.

On peut mettre de l'*italique* et du **gras**.

On peut mettre des maths en les encadrant entre dollar, accent grave à gauche et accent grave, dollar à droite. Ça donne des formules de ce genre:

- $`f(x)=x^3-2x^2+5x+1+\sqrt{x+3}+\dfrac{1-x^2}{1+x^2}`$

